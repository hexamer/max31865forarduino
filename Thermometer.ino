//////////////////////////////////////////////////////////////////////////////////////////////////
//
// MAX31865 Thermometer for Arduino
//
// Copyright (c) 2014 by Aaron Hexamer
//
// This file is part of the MAX31865 Thermometer for Arduino program.
//
// MAX31865 Thermometer for Arduino is free software: you can 
// redistribute it and/or modify it under the terms of the GNU General Public 
// License as published by the Free Software Foundation, either version 3 of 
// the License, or (at your option) any later version.
//
// MAX31865 Thermometer for Arduino is distributed in the hope that 
// it will be useful,but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with MAX31865 Thermometer for Arduino.  If not, see <http://www.gnu.org/licenses/>.
//
// Module: Thermometer.ino
//
//////////////////////////////////////////////////////////////////////////////////////////////////

// include the library code:
#include <LiquidCrystal.h>
#include <Wire.h>
#include "RTClib.h"
#include <SD.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

const int nCSpin = A2;
const int MOSIpin = A0;
const int MISOpin = A3;
const int CLKpin = A1;

const int SDnCsPin = 10;
const int SDMosiPin = 11;
const int SDMisoPin = 12;
const int SDClkPin = 13;

const unsigned char configReadAddr = 0x00;
const unsigned char configWriteAddr = 0x80;
const unsigned char rtdReadAddr = 0x01;
const unsigned char faultReadAddr = 0x07;

RTC_DS1307 RTC;

File logFile;
unsigned long lastLogTime = 0;

void setup()
{
  
   unsigned char cfg;
   
   Serial.begin(9600);
   
   // set up the LCD's number of columns and rows: 
   lcd.begin(16, 2);
   // Print a message to the LCD.
   lcd.print("Thermometer");
   
   // Setup the RTC
   Wire.begin();
   RTC.begin();
 
   if (!RTC.isrunning()) 
   {
      Serial.println("RTC is NOT running!");
      // following line sets the RTC to the date & time this sketch was compiled
      // uncomment it & upload to set the time, date and start run the RTC!
      RTC.adjust(DateTime(__DATE__, __TIME__));
   }
  
   digitalWrite( nCSpin, HIGH );
   pinMode( nCSpin, OUTPUT );
  
   digitalWrite( CLKpin, LOW );
   pinMode( CLKpin, OUTPUT );
  
   pinMode( MISOpin, INPUT );
  
   digitalWrite( MOSIpin, LOW );
   pinMode( MOSIpin, OUTPUT );
   
   // Clear faults
   WriteConfig( 0x02 );

   // Setup Configuration: 3 wire; Turn on Vbias; Manual Conversion; 60 Hz filter
   WriteConfig( 0x90 );
   
   cfg = ReadConfig();

#if defined(DEBUG)  
Serial.print( "cfg: " );
Serial.print( cfg, HEX );
Serial.print( "\n" );
#endif

   // Setup the SD card
   digitalWrite( SDnCsPin, HIGH );
   pinMode( SDnCsPin, OUTPUT );
   
   if (!SD.begin(SDnCsPin))
   {
      lcd.setCursor(0, 0);
      lcd.print("SD card failed,");
      lcd.setCursor(0, 1);
      lcd.print("or not present.");
      delay( 3000 );
      lcd.clear();
   }
   else
   {
      Serial.println("SD card initialized.");
      // TODO Check and print SD card stats

      // Generate a logfile name.  Use Unix epoch time in hex to get a unique filename
      // since logfile names can't be longer than 8.3
      char fileName[13];
      DateTime now = RTC.now();
      sprintf( fileName, "%lX.log", now.unixtime());
      logFile = SD.open(fileName, FILE_WRITE);
      
      if(!logFile)
      {
         lcd.setCursor(0, 0);
         lcd.print("Failed to open:");
         lcd.setCursor(0, 1);
         lcd.print(fileName);
         delay( 3000 );
         lcd.clear();
      }
      else
      {
         logFile.print( "Time, Temperature (C), Temperature (F)" );
         logFile.flush();
      }
   }  
}

void loop() 
{
  
   unsigned int resRatio_u0p16 = 0;
   int temperature_C_s7p8 = 0;
   int temperature_F_s8p7 = 0;
   int tempx10 = 0;
   char temperatureStringC[7];
   char temperatureStringF[7];
   
   resRatio_u0p16 = ReadResistanceRatio();
   
   CalculateTemperature( resRatio_u0p16, &temperature_C_s7p8, &temperature_F_s8p7 );

   tempx10 = ((long)temperature_C_s7p8 * 10L)>>8;

#if defined(DEBUG)   
Serial.print( "tempx10: " );
Serial.print( tempx10, HEX );
Serial.print( "\n" ); 
#endif
   
   sprintf( temperatureStringC, "%d.%d", tempx10/10, abs(tempx10%10) );
   
   lcd.setCursor(0, 1);
   lcd.print(temperatureStringC);
   lcd.write( 0xDF ); // Write the degree symbol
   lcd.print( "C " );
   
   tempx10 = ((long)temperature_F_s8p7 * 10L)>>7;
   sprintf( temperatureStringF, "%d.%d", tempx10/10, abs(tempx10%10) );
   
   lcd.print(temperatureStringF);
   lcd.write( 0xDF ); // Write the degree symbol
   lcd.print( "F     " );

   DateTime now = RTC.now();
#if defined(DEBUG)   
Serial.print(now.year(), DEC);
Serial.print('/');
Serial.print(now.month(), DEC);
Serial.print('/');
Serial.print(now.day(), DEC);
Serial.print(' ');
Serial.print(now.hour(), DEC);
Serial.print(':');
Serial.print(now.minute(), DEC);
Serial.print(':');
Serial.print(now.second(), DEC);
Serial.println();
#endif

   lcd.setCursor(0, 0);
   lcd.print(now.month(), DEC);
   lcd.print('/');
   lcd.print(now.day(), DEC);
   lcd.print('/');
   lcd.print((now.year()%100), DEC);
   lcd.print(' ');
   lcd.print(now.hour(), DEC);
   lcd.print(':');
   if (now.minute() < 10 )
   {
      lcd.print( '0' );   
   }
   lcd.print(now.minute(), DEC);
   
   if (logFile)
   {     
      if (now.unixtime() - lastLogTime >= 60)
      {
         lastLogTime = now.unixtime();
         
         logFile.println("");
     
         logFile.print(now.month(), DEC);
         logFile.print('/');
         logFile.print(now.day(), DEC);
         logFile.print('/');
         logFile.print((now.year()%100), DEC);
         logFile.print(' ');
         logFile.print(now.hour(), DEC);
         logFile.print(':');
         if (now.minute() < 10 )
         {
            logFile.print( '0' );   
         }
         logFile.print(now.minute(), DEC);
         logFile.print(':');
         if (now.second() < 10 )
         {
            logFile.print( '0' );   
         }
         logFile.print(now.second(), DEC);
   
         logFile.print(",");
         logFile.print(temperatureStringC);
   
         logFile.print(",");
         logFile.print(temperatureStringF);
   
         logFile.flush();
         lcd.setCursor(15, 1);
         lcd.write(0x20);
      }
      else if (now.unixtime() - lastLogTime >= (60-3)) // Warning time
      {
         lcd.setCursor(15, 1);
         lcd.write(0xFF);
      }
  
   }
  
   delay( 1000 );
  
}

void WriteConfig( unsigned char cfg )
{
 
   digitalWrite( nCSpin, LOW );
   shiftOut( MOSIpin, CLKpin, MSBFIRST, configWriteAddr );
   shiftOut( MOSIpin, CLKpin, MSBFIRST, cfg );
   digitalWrite( nCSpin, HIGH );
  
}

unsigned char ReadConfig( void )
{
  
   unsigned char cfg;
 
   digitalWrite( nCSpin, LOW );
   shiftOut( MOSIpin, CLKpin, MSBFIRST, configReadAddr );
   cfg = shiftIn( MISOpin, CLKpin, MSBFIRST );
   digitalWrite( nCSpin, HIGH );
   
   return cfg;
}

unsigned char ReadFault( void )
{
  
   unsigned char flt;
 
   digitalWrite( nCSpin, LOW );
   shiftOut( MOSIpin, CLKpin, MSBFIRST, faultReadAddr );
   flt = shiftIn( MISOpin, CLKpin, MSBFIRST );
   digitalWrite( nCSpin, HIGH );
   
   return flt;
}

unsigned int ReadResistanceRatio( void )
{
   unsigned char cfg;
   unsigned char flt;
   unsigned char upperRR;
   unsigned char lowerRR;
   
   cfg = ReadConfig();
   cfg |= 0x20;              // Set the conversion bit

   WriteConfig( cfg );

   delay( 100 );

   digitalWrite( nCSpin, LOW );
   shiftOut( MOSIpin, CLKpin, MSBFIRST, rtdReadAddr );
   upperRR = shiftIn( MISOpin, CLKpin, MSBFIRST );
   lowerRR = shiftIn( MISOpin, CLKpin, MSBFIRST );
   digitalWrite( nCSpin, HIGH );
   
   if (lowerRR & 0x01)
   {
       flt = ReadFault();
       Serial.print( "fault: " );
       Serial.print( flt, HEX );
       Serial.print( "\n" );
       
       // Clear faults
       cfg = ReadConfig();
       cfg |= 0x02;              // Set fault clear bit
       WriteConfig( cfg );
   }
   
   return (upperRR<<8 | lowerRR);
}

const unsigned short refRes_9p7 = 0xC800; // Reference resistance 400 ohms

typedef struct 
{
    unsigned long resistance_u8p24; // only good for up to 423C/793F
    unsigned int slope_u2p14; // as u2p14
    long temperature_s7p24; // as s7p24 // only good for -128 to 127C
} rtdTableEntry;

const rtdTableEntry rtdTable[33] = 
{
    { 0x6015FD8A,	0xA35B,	0xF5FFEC57 },
    { 0x6187A5AE,	0xA389,	0xF9AF73C1 },
    { 0x62F94DD2,	0xA3B6,	0xFD6003EF },
    { 0x646AF5F6,	0xA3E4,	0x01119CE0 },
    { 0x65DC9E1B,	0xA412,	0x04C43E96 },
    { 0x674E463F,	0xA440,	0x0877E9B7 },
    { 0x68BFEE63,	0xA46F,	0x0C2C9EEC },
    { 0x6A319687,	0xA49C,	0x0FE2617C },
    { 0x6BA33EAB,	0xA4CC,	0x13992A30 },
    { 0x6D14E6CF,	0xA4FB,	0x17510624 },
    { 0x6E868EF3,	0xA529,	0x1B09EECB },
    { 0x6FF83717,	0xA559,	0x1EC3E425 },
    { 0x7169DF3B,	0xA587,	0x227EECBF },
    { 0x72DB875F,	0xA5B7,	0x263B020C },
    { 0x744D2F83,	0xA5E7,	0x29F82A99 },
    { 0x75BED7A7,	0xA616,	0x2DB66666 },
    { 0x77307FCB,	0xA646,	0x3175B573 },
    { 0x78A227EF,	0xA675,	0x353617C1 },
    { 0x7A13D013,	0xA6A6,	0x38F78D4F },
    { 0x7B857837,	0xA6D7,	0x3CBA1CAC },
    { 0x7CF7205B,	0xA707,	0x407DC5D6 },
    { 0x7E68C87F,	0xA738,	0x44428240 },
    { 0x7FDA70A3,	0xA768,	0x48085879 },
    { 0x814C18C7,	0xA799,	0x4BCF487F },
    { 0x82BDC0EB,	0xA7CB,	0x4F975254 },
    { 0x842F690F,	0xA7FC,	0x53607C84 },
    { 0x85A11134,	0xA82D,	0x572AC083 },
    { 0x8712B958,	0xA860,	0x5AF61E4F },
    { 0x8884617C,	0xA890,	0x5EC2A305 },
    { 0x89F609A0,	0xA8C1,	0x62904189 },
    { 0x8B67B1C4,	0xA8F4,	0x665EF9DB },
    { 0x8CD959E8,	0xA92D,	0x6A2ED916 },
    { 0x8E4B020C,	0x0,	0x6E000000 }
};

int CalculateTemperature( unsigned int resRatio_u0p16, int* temperature_C, int* temperature_F )
{
  
    // First calculate resistance
    unsigned long resistance_u8p24 = 0; // only good for up to 423C/793F
    unsigned int deltaRes_u1p15 = 0;
    unsigned long deltaTemp_u8p24 = 0;
    long temperature_C_s7p24 = 0;
    int idx;

#if defined(DEBUG)    
Serial.print( "resRatio_u0p16: " );
Serial.print( resRatio_u0p16, DEC );
Serial.print( "\n" );
#endif
    
    resistance_u8p24 = ((unsigned long)resRatio_u0p16 * (unsigned long)refRes_9p7)<<1;

#if defined(DEBUG)    
//resistance_u8p24 -= 184549376; // take off 11 ohms to test negative C
Serial.print( "resistance_u8p24: " );
Serial.print( resistance_u8p24, DEC );
Serial.print( "\n" );
#endif
    
    // First make sure the resistance is not outside the span of the table
    if (resistance_u8p24 < rtdTable[0].resistance_u8p24 || resistance_u8p24 >= rtdTable[sizeof(rtdTable)/sizeof(rtdTableEntry)-1].resistance_u8p24)
    {
        return -1;
    }
    
    // Find resistance in the table; optimize later: could do mru and binary search
    for (idx = 0; idx < sizeof(rtdTable)/sizeof(rtdTableEntry); idx++)
    {
        if ( resistance_u8p24 < rtdTable[idx+1].resistance_u8p24 )
        {
            break;
        }
    }

#if defined(DEBUG)    
Serial.print( "idx: " );
Serial.print( idx, DEC );
Serial.print( "\n" );
#endif
    
    deltaRes_u1p15 = (resistance_u8p24 - rtdTable[idx].resistance_u8p24)>>9;
    
#if defined(DEBUG)    
Serial.print( "deltaRes_u1p15: " );
Serial.print( deltaRes_u1p15, DEC );
Serial.print( "\n" );
#endif
    
    deltaTemp_u8p24 = ((unsigned long)deltaRes_u1p15 * (unsigned long)rtdTable[idx].slope_u2p14)>>5;

#if defined(DEBUG)    
Serial.print( "deltaTemp_u8p24: " );
Serial.print( deltaTemp_u8p24, DEC );
Serial.print( "\n" );      
#endif
    
    temperature_C_s7p24 = rtdTable[idx].temperature_s7p24 + deltaTemp_u8p24;

#if defined(DEBUG)    
Serial.print( "temperature_C_s7p24: " );
Serial.print( temperature_C_s7p24, DEC );
Serial.print( "\n" );      
#endif
  
    *temperature_C = temperature_C_s7p24 >> 16;

#if defined(DEBUG)    
Serial.print( "*temperature_C: " );
Serial.print( *temperature_C, DEC );
Serial.print( "\n" );      
#endif
    
    *temperature_F = CelciusToFarenheit( *temperature_C );

#if defined(DEBUG)    
Serial.print( "*temperature_F: " );
Serial.print( *temperature_F, DEC );
Serial.print( "\n" );      
#endif
    
    return 0;
  
}

// C to F constants
const unsigned int NineFifths_8p8 = 461; // 9/5 as an 8p8 (1.80078125)
const unsigned long ThirtyTwo_16p16 = 0x200000; // 32 as an 16p16 (exact)

unsigned int CelciusToFarenheit( int temperature_C_s7p8 )
{
     long temperature_F_s15p16 = (long)temperature_C_s7p8 * (long)NineFifths_8p8 + ThirtyTwo_16p16;   
#if defined(DEBUG)     
Serial.print( "temperature_F_s15p16: " );
Serial.print( temperature_F_s15p16, DEC );
Serial.print( "\n" );  
#endif
     return (temperature_F_s15p16 >> 9);
}
